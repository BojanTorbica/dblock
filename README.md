# README #

### What is this repository for? ###

* Simple application to simulate database access based upon face expression recognition of a user as passcode.
* Depends on: https://bitbucket.org/BojanTorbica/dblockapplicationpreloader
* 0.5

### Running the application ###

* In order to run the application, you have to download the source code and run it from your IDE of choice, that's it! :) (The app was originally done in NetBeans)
* The repository might be sporadically updated with new features and enhancements to this original prototype.
* If you download the code, change it, enhance it as you see fit and have fun! :)