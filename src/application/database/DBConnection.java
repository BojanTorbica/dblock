/*
 * Copyright (C) 2015 no-one
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Use and modify this application as you see fit! :)
 */
package application.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 */
public class DBConnection {
    private Connection dbConnection = null;
 
    public Connection connect() {
        try {
            Class connection = Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Connection to server established " + connection);
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
            System.out.println("Connection to server failed " + e);
        }
        
        String dbURL = "jdbc:mysql://localhost:3306/userbase";
        try {
            dbConnection = DriverManager.getConnection(dbURL, "root", "l_0c*41H0_5T");
            System.out.println("Database connection established on: " + dbURL);
        } catch (SQLException e) {
            System.out.println("Database connection failed on: " + dbURL);
            System.out.println(e.getErrorCode() + " " + e.getSQLState() + e.getMessage() + " " + e);
        }
        
        return this.dbConnection;
    }
    
    /**
     * @param querry
     * @param connection
     * @param isRetrieve
     * @return Auto-incremented keys (the id column)
     * @throws SQLException 
     */
    public static ResultSet dbQuery(String querry, Connection connection, boolean isRetrieve) throws SQLException {
        Statement statement = connection.createStatement();
        if (!isRetrieve) {
            statement.executeUpdate(querry, Statement.RETURN_GENERATED_KEYS);
            return statement.getGeneratedKeys();
        } else {
            return statement.executeQuery(querry);
        }
    }
}