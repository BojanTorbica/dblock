/*
 * Copyright (C) 2015 no-one
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Use and modify this application as you see fit! :)
 */
package application;

/**
 * Java imports
 */
import java.io.IOException;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * JavaFX imports
 */
import javafx.application.Application;
import javafx.application.Platform;
import javafx.application.Preloader;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.stage.Modality;

/**
 * Application packages imports
 */
import application.database.DBConnection;
import application.thread.DBCamThread;
import model.DBUserRegistry;

/*
 Main application class
*/
public class DBApplicationCore extends Application {
    /**
     * Application singleton instance.
     *
     * Since JavaFX does not have a clear mechanism for allowing singleton applications,
     * following constructor is provided in order to prevent an eventual second instance
     * of the same main window.
     */
    private static DBApplicationCore instance = null;

    public DBApplicationCore() {
        super();
        synchronized(DBApplicationCore.class) {
            if (instance != null) throw new UnsupportedOperationException (
                    getClass() + " is singleton but constructor called more than once");
            DBApplicationCore.instance = this;
        }
    }
    /**
     * Database connection.
     */
    public static Connection connection = new DBConnection().connect();
    
    /**
     * User registry
     */
    public static DBUserRegistry registry = new DBUserRegistry();
    
    /**
     * Input video stream
     */
    public static DBCamThread thread = new DBCamThread();
    public static Thread t = null;

    /**
     * Application windows (or scenes in JavaFX context and terminology).
     */
    Scene mainScene, entryScene, loginScene, newUserScene, verifyNewScene, verifyExistsScene, messageScene = null;
    Stage main, entry, login, newUser, verifyNewStage, verifyExistsStage, messageStage = null;
    
    /**
    * This method is used to simulate a long application startup time.
    * This entire sample is taken from Oracle documentation on preloaders.
    */
    private final BooleanProperty ready = new SimpleBooleanProperty(false);
    public static BooleanProperty exitApp = new SimpleBooleanProperty(false);
    public static BooleanProperty logIn = new SimpleBooleanProperty(false);
    public static BooleanProperty userNew = new SimpleBooleanProperty(false);
    public static BooleanProperty userNewVerification = new SimpleBooleanProperty(false);
    public static BooleanProperty loginVerification = new SimpleBooleanProperty(false);
    public static BooleanProperty message = new SimpleBooleanProperty(false);
    public static BooleanProperty successLogin = new SimpleBooleanProperty(false);

    private void longStart() {
        //simulate long init in background
        Task task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                int max = 10;
                for (int i = 1; i <= max; i++) {
                    Thread.sleep(300);
                    // Send progress to preloader
                    notifyPreloader(new Preloader.ProgressNotification(((double) i)/max));
                }
                // After init is ready, the app is ready to be shown
                // Do this before hiding the preloader stage to prevent the
                // app from exiting prematurely
                ready.setValue(true);
                notifyPreloader(new Preloader.StateChangeNotification(Preloader.StateChangeNotification.Type.BEFORE_START));

                return null;
            }
        };
        new Thread(task).start();
    }

    /**
     * The main entry point for all JavaFX applications.
     * The start method is called after the init method has returned,
     * and after the system is ready for the application to begin running.
     * <p>
     * <p>
     * NOTE: This method is called on the JavaFX Application Thread.
     * </p>
     *
     * @param primaryStage the primary stage for this application, onto which
     *                     the application scene can be set. The primary stage will be embedded in
     *                     the browser if the application was launched as an applet.
     *                     Applications may create other stages, if needed, but they will not be
     *                     primary stages and will not be embedded in the browser.
     * @throws java.lang.Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        // Initiate simulated long startup sequence
        this.longStart();
        this.entry = primaryStage;
        
        Parent mainRoot = FXMLLoader.load(getClass().getResource("/view/DBMain.fxml"));
        Parent entryRoot = FXMLLoader.load(getClass().getResource("/view/DBEntry.fxml"));
        
        mainScene = new Scene(mainRoot);
        entryScene = new Scene(entryRoot);

        /* After the app is ready, show the stage */
        ready.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue) {
                Platform.runLater(() -> {
                    this.entry.getIcons().addAll(new Image("/resources/images/main.png"));
                    this.entry.initStyle(StageStyle.UNDECORATED);
                    this.entry.setScene(entryScene);
                    this.entry.setTitle("DB Lock Login");
                    this.entry.show();
                });
            }
        });
        
        /* If the user has clicked the Log In button on the entry screen, show user login screen, else if he hits cancel, return */
        logIn.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue) {
                if (this.entry.showingProperty().get()) {
                    Parent loginRoot = null;
                    try {
                        loginRoot = FXMLLoader.load(getClass().getResource("/view/DBUserLogInDialogView.fxml"));
                    } catch (IOException ex) {
                        Logger.getLogger(DBApplicationCore.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    loginScene = new Scene(loginRoot);
                    
                    this.login = new Stage();
                    this.login.initModality(Modality.APPLICATION_MODAL);
                    this.login.centerOnScreen();
                    this.login.getIcons().addAll(new Image("/resources/images/main.png"));
                    this.login.setScene(loginScene);
                    this.login.setTitle("DB Lock Login");
                    
                    this.login.setOnCloseRequest((WindowEvent event) -> {
                        DBApplicationCore.logIn.set(false);
                    });
                    
                    this.login.show();
                }
            } else {
                if (this.login.showingProperty().get()) {
                    this.login.close();
                }
            }
        });
        
        /* Show the facial expression passcode verification dialog for he currently logged in user */
        loginVerification.addListener((ObservableValue<? extends Boolean> observabl, Boolean oldValue, Boolean newValue) -> {
            if (newValue) {
                Parent verifyExistsRoot = null;
                    try {
                        verifyExistsRoot = FXMLLoader.load(getClass().getResource("/view/DBUserLogInVerificationDialogView.fxml"));
                    } catch (IOException ex) {
                        Logger.getLogger(DBApplicationCore.class.getName()).log(Level.SEVERE, null, ex);
                    }
                verifyExistsScene = new Scene(verifyExistsRoot);
                    
                this.verifyExistsStage = new Stage();
                this.verifyExistsStage.initModality(Modality.APPLICATION_MODAL);
                this.verifyExistsStage.centerOnScreen();
                this.verifyExistsStage.getIcons().addAll(new Image("/resources/images/main.png"));
                this.verifyExistsStage.setScene(verifyExistsScene);
                this.verifyExistsStage.setTitle("DB Lock Login");
                
                this.verifyExistsStage.setOnCloseRequest((WindowEvent event) -> {
                    DBApplicationCore.loginVerification.set(false);
                });
                
                this.verifyExistsStage.show();
            } else {
                if (this.verifyExistsStage.showingProperty().get()) {
                    this.verifyExistsStage.close();
                }
            }
        });
        
        /* If the user has clicked the Register button on the entry screen, show new user screen, else if he hits cancel, return */
        userNew.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue) {
                if (this.entry.showingProperty().get()) {
                    Parent newUserRoot = null;
                    try {
                        newUserRoot = FXMLLoader.load(getClass().getResource("/view/DBUserNewDialogView.fxml"));
                    } catch (IOException ex) {
                        Logger.getLogger(DBApplicationCore.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    newUserScene = new Scene(newUserRoot);
                    
                    this.newUser = new Stage();
                    this.newUser.initModality(Modality.APPLICATION_MODAL);
                    this.newUser.centerOnScreen();
                    this.newUser.getIcons().addAll(new Image("/resources/images/main.png"));
                    this.newUser.setScene(newUserScene);
                    this.newUser.setTitle("DB Lock user registration");
                    
                    this.newUser.setOnCloseRequest((WindowEvent event) -> {
                        DBApplicationCore.userNew.set(false);
                    });
                    
                    this.newUser.show();
                }
            } else {
                if (this.newUser.showingProperty().get()) {
                    this.newUser.close();
                }
            }
        });
        
        /* Show the facial expression passcode registration dialog */
        userNewVerification.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue) {
                if (this.entry.showingProperty().get()) {
                    Parent newUserVerificationRoot = null;
                    try {
                        newUserVerificationRoot = FXMLLoader.load(getClass().getResource("/view/DBUserNewVerificationView.fxml"));
                    } catch (IOException ex) {
                        Logger.getLogger(DBApplicationCore.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    verifyNewScene = new Scene(newUserVerificationRoot);
                    
                    this.verifyNewStage = new Stage();
                    this.verifyNewStage.initModality(Modality.APPLICATION_MODAL);
                    this.verifyNewStage.centerOnScreen();
                    this.verifyNewStage.getIcons().addAll(new Image("/resources/images/main.png"));
                    this.verifyNewStage.setScene(verifyNewScene);
                    this.verifyNewStage.setTitle("DB Lock user registration");
                    
                    this.verifyNewStage.setOnCloseRequest((WindowEvent event) -> {
                        DBApplicationCore.userNewVerification.set(false);
                    });
                    
                    this.verifyNewStage.show();
                }
            } else {
                if (this.verifyNewStage.showingProperty().get()) {
                    this.verifyNewStage.close();
                }
            }
        });
        
        /* ========================================================================================== */
        message.addListener((ObservableValue<? extends Boolean> observabl, Boolean oldValue, Boolean newValue) -> {
            if (newValue) {
                Parent verifiedScreen = null;
                try {
                    verifiedScreen = FXMLLoader.load(getClass().getResource("/view/DBUserMessage.fxml"));
                } catch (IOException ex) {
                    Logger.getLogger(DBApplicationCore.class.getName()).log(Level.SEVERE, null, ex);
                }
                this.messageScene = new Scene(verifiedScreen);

                this.messageStage = new Stage();
                this.messageStage.initModality(Modality.APPLICATION_MODAL);
                this.messageStage.centerOnScreen();
                this.messageStage.getIcons().addAll(new Image("/resources/images/main.png"));
                this.messageStage.setScene(this.messageScene);
                this.messageStage.setTitle("DB Lock Login");

                this.messageStage.setOnCloseRequest((WindowEvent event) -> {
                    DBApplicationCore.message.set(false);
                });

                this.messageStage.show();
            } else {
                if (this.messageStage.showingProperty().get()) {
                    this.messageStage.close();
                }
            }
        });
        
        /* If the user has successfully loged in, show main application window */
        successLogin.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue) {
                this.entry.close();
                
                this.main = new Stage();
                this.main.setScene(mainScene);
                this.main.getIcons().addAll(new Image("/resources/images/main.png"));
                this.main.initStyle(StageStyle.UNIFIED);
                this.main.setTitle("DB Lock");
                this.main.show();
            }
        });
        
        /* If the user has clicked the exit button on the entry screen, terminate application */
        exitApp.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue) {
                if (this.entry.showingProperty().get()) {
                    //this.entry.close();
                    Platform.exit();
                }
            }
        });
    }
    
    public static void main(String[] args) {
        launch(args);
    }
}