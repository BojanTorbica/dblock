/*
 * Copyright (C) 2015 no-one
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Use and modify this application as you see fit! :)
 */
package application.thread;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;
import static org.bytedeco.javacpp.opencv_highgui.waitKey;
import static org.bytedeco.javacpp.opencv_imgproc.resize;
import org.bytedeco.javacpp.opencv_videoio.VideoCapture;

import application.detection.DBFaceDetect;

/**
 *
 */
public class DBCamThread implements Runnable {
    protected volatile boolean runnable = false;
    
    private VideoCapture inputStream = null;
    private Mat frame = new Mat();
    private Mat currentFrame = new Mat();
    private Mat[] frames = null;
    private ImageView imageView = null;
    private Rect boundingBox = null;
    
    private final String haarCascadeFace = "src/resources/classifiers/haarcascades/haarcascade_frontalface_default.xml";
    private final String lbpCascadeFace = "src/resources/classifiers/lbpcascades/lbpcascade_frontalcatface.xml";
    private DBFaceDetect faceDetector = null;
    
    public DBCamThread() {
        this.inputStream = new VideoCapture(0);
        this.faceDetector = new DBFaceDetect(this.haarCascadeFace);
        //this.faceDetector = new DBFaceDetect(this.lbpCascadeFile);
    }
    
    @Override
    public void run() {
        synchronized(this) {
            while (runnable) {
                if (inputStream.grab()) {
                    this.start();
                }
            }
        }
    }
    
    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }
    
    public void start() {
        try { 
            this.inputStream.retrieve(this.frame);
            resize(this.frame, this.frame, new Size(400, 300));
            if (!this.frame.empty()) {
                this.currentFrame = this.frame;
            }

            /* Attempt to detect a face */
            this.frames = this.faceDetector.detectFace(frame);
            this.boundingBox = this.faceDetector.getBoundingBoxForFace();

            imwrite("src/resources/tmp/newUserExpressionCapture.bmp", this.frames[0]);

            File file = new File("src/resources/tmp/newUserExpressionCapture.bmp");
            Image image = new Image(file.toURI().toString());
            this.imageView.setImage(image);

            waitKey(30);
        } catch (Exception ex) {
            Logger.getLogger(DBCamThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean match(Mat frame, Mat[] template, int matchMethod) {
        return this.faceDetector.matchToTemplateFaceImage(frame, template, matchMethod);
    }
    
    public Mat[] getTemplates(String username, int numSegments) {
        return this.faceDetector.getTemplates(username, numSegments);
    }
    
    public Mat[] segmentTemplate(Mat template, int numSegments, String username) {
        return this.faceDetector.segmentTemplateImage(template, numSegments, username);
    }
    
    public boolean isThreadRunning() {
        return this.runnable;
    }
    
    public Mat getCurrentFrame() {
        Mat toReturn = new Mat();
        if (!this.currentFrame.empty()) {
            toReturn = this.currentFrame;
        }
        return toReturn;
    }
    
    public Mat getDetectedFace() {
        Mat toReturn = new Mat();
        if (!(this.frames[1].empty())) {
            toReturn = this.frames[1];
        }
        return toReturn;
    }
    
    public Rect getBoundingBox() {
        return this.boundingBox;
    }
    
    public void setRunnable(boolean isRunnable) {
        this.runnable = isRunnable;
    }
    
    public void openStream() {
        if (!this.inputStream.isOpened()) {
            this.inputStream.open(0);
        }
    }
    
    public void closeStream() {
        this.inputStream.release();
    }
}