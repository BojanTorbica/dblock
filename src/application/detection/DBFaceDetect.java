/*
 * Copyright (C) 2015 no-one
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Use and modify this application as you see fit! :)
 */
package application.detection;

import application.DBApplicationCore;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.*;

import org.bytedeco.javacpp.opencv_objdetect.CascadeClassifier;
import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_highgui.*;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;
import static org.bytedeco.javacpp.opencv_imgproc.*;
import static org.bytedeco.javacpp.opencv_objdetect.CV_HAAR_FIND_BIGGEST_OBJECT;

/**
 *
 */
public class DBFaceDetect {
    private CascadeClassifier faceCascade = new CascadeClassifier();
    
    private int[] matchMethod = {CV_TM_SQDIFF,
                                 CV_TM_SQDIFF_NORMED,
                                 CV_TM_CCORR,
                                 CV_TM_CCORR_NORMED,
                                 CV_TM_CCOEFF,
                                 CV_TM_CCOEFF_NORMED};
    
    private Mat[] array = new Mat[2];
    private Mat[] templates = null;
    
    private Rect boundingBox = new Rect();
    private RectVector faces = new RectVector();
    
    public DBFaceDetect(String faceCascade) {
        this.faceCascade.load(faceCascade);
    }
    
    public Mat[] detectFace(Mat frame) {
        this.array[0] = frame.clone();
        this.array[1] = new Mat();
        
        //cvtColor(this.array[0], this.array[0], CV_BGR2GRAY);
        //GaussianBlur(this.array[0], this.array[0], new Size(3, 3), 0, 0, BORDER_DEFAULT);
        
        this.faceCascade.detectMultiScale(this.array[0], this.faces, 1.1, 8, CV_HAAR_FIND_BIGGEST_OBJECT, new Size(120, 120), new Size(frame.size()));
        
        for (int i = 0; i < faces.size(); i++) {
            this.array[1] = new Mat(this.array[0], this.faces.get(i));
            cvtColor(this.array[1], this.array[1], CV_BGR2GRAY);
            
            rectangle(this.array[0], this.faces.get(i), new Scalar(255.0, 0.0, 0.0, 1.0), 2, CV_AA, 0);
            this.setBoundingBoxForFace(this.faces.get(i));
        }
        
        return this.array;
    }
    
    public Mat[] segmentTemplateImage(Mat template, int numSegments, String username) {
        this.templates = new Mat[numSegments*numSegments];
        
        int width = template.cols();
        int height = template.rows();
        
        System.out.println("Width: " + width);
        System.out.println("Height: " + height);
        
        int size = (int)(template.size().width() / numSegments) * numSegments;
        System.out.println("Size: " + size);
        resize(template, template, new Size(size, size));
        DBApplicationCore.registry.setSizeOfROI(size);

        for (int y = 0; y < numSegments; y++) {
            for (int x = 0; x < numSegments; x++) {
                Point ul = new Point(x * (int)(width / numSegments), y * (int)(width / numSegments));
                Point dr = new Point((x + 1) * (int)(width / numSegments), (y + 1) * (int)(width / numSegments));

                System.out.println("Upper left x: " + ul.x() + " " + ul.y());
                System.out.println("Down right x: " + dr.x() + " " + dr.y());
                
                Rect roi = new Rect(ul, dr);
                this.templates[x + y * numSegments] = new Mat(template, roi);
                
                //equalizeHist(this.templates[x + y * numSegments], this.templates[x + y * numSegments]);
            }
        }

        for (int i = 0; i < numSegments * numSegments; i++) {
            String imageName = "src/resources/users/" + username + "/templates/template_0" + i + ".bmp";
            imwrite(imageName, this.templates[i]);
        }
                
        return this.templates;
    }
    
    public Mat[] getTemplates(String username, int numSegments) {
        if (this.templates == null) {
            this.templates = new Mat[numSegments * numSegments];
            for (int i = 0; i < numSegments * numSegments; i++) {
                String imageName = "src/resources/users/" + username + "/templates/template_0" + i + ".bmp";
                this.templates[i] = imread(imageName, 0);
                System.out.println("Template type: " + this.templates[i].type());
            }
        }
        return this.templates;
    }
    
    public boolean matchToTemplateFaceImage(Mat frame, Mat[] templateArray, int matchMethod) {
        Mat source = frame.clone();
        //equalizeHist(source, source);
        
        int size = 0;
        try {
            String stream = this.readStream(DBApplicationCore.registry.getCurrentUser());
            String[] split = stream.split("Size: ");
            size = Integer.parseInt(split[1]);
        } catch (IOException ex) {
            Logger.getLogger(DBFaceDetect.class.getName()).log(Level.SEVERE, null, ex);
        }
        resize(source, source, new Size(size, size));

        Mat result = new Mat();
        int resultRows = source.rows() - templateArray[0].rows() + 1;
        int resultCols = source.cols() - templateArray[0].cols() + 1;
        result.create(resultRows, resultCols, source.type());
        
        int count = 0;
        Point[] points = new Point[templates.length];
        
        System.out.println("Result type: " + result.type());
        System.out.println("Source type: " + source.type());
        
        if (matchMethod <= -1) {
            matchMethod = this.matchMethod[0];
        }
        
        for (int t = 0; t < templates.length; t++) {
            matchTemplate(source, templates[t], result, matchMethod);
            normalize(result, result, 0, 1, NORM_MINMAX, -1, new Mat());

            double[] minValue = new double[1]; 
            double[] maxValue = new double[1];
            Point minLoc = new Point();
            Point maxLoc = new Point();
            Point matchLoc = new Point();

            minMaxLoc(result, minValue, maxValue, minLoc, maxLoc, new Mat());
            System.out.println("Min: " + t + " " + java.util.Arrays.toString(minValue));
            System.out.println("Max: " + t + " " + java.util.Arrays.toString(maxValue));
            if (matchMethod == this.matchMethod[0] || matchMethod == this.matchMethod[1]) {
                matchLoc = minLoc;
                //System.out.println("Match point (min): " + matchLoc.x() + " " + matchLoc.y());
            } else {
                matchLoc = maxLoc;
                //System.out.println("Match point (max): " + matchLoc.x() + " " + matchLoc.y());
            }

            rectangle(source, matchLoc, new Point(matchLoc.x() + templateArray[t].cols(), 
                      matchLoc.y() + templateArray[t].rows()), new Scalar(0.0, 0.0, 0.0, 1.0), 2, 8, 0);
            putText(source, "t:" + t, new Point(matchLoc.x(), matchLoc.y()), FONT_HERSHEY_COMPLEX_SMALL, 0.4, 
                    new Scalar(255, 255, 255, 1.0));
            
            Mat matchedROI = new Mat(source, new Rect(matchLoc, new Point(matchLoc.x() + templateArray[t].cols(), 
                                    matchLoc.y() + templateArray[t].rows())));
            
            Mat diff = new Mat(matchedROI.rows(), matchedROI.cols(), matchedROI.type());
            compare(matchedROI, templateArray[t], diff, CMP_NE);

            int nonZero = countNonZero(diff);
            System.out.println("Non-zero diffs: " + t + " " + nonZero);
        }

        imshow("src", source);
        
        return true;
    }
    
    private String readStream(String username) throws IOException {
        Path path = Paths.get("src/resources/users/" + username + "/", username + "_face_bounding_box.txt");
        String returnString = "";
        try(java.util.stream.Stream<String> lines = Files.lines(path)) {
             Optional<String> hasWord = lines.filter(s -> s.contains("Size:")).findFirst();
             if(hasWord.isPresent()){
                 returnString = hasWord.get();
             }
         }
        return returnString;
    }
    
    public void setBoundingBoxForFace(Rect boundingBox) {
        this.boundingBox = boundingBox;
    }
    
    public Rect getBoundingBoxForFace() {
        return this.boundingBox;
    }
}