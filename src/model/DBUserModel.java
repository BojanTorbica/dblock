/*
 * Copyright (C) 2015 no-one
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Use and modify this application as you see fit! :)
 */
package model;

import java.util.Random;

/**
 * Model class to hold user data such as, username, pin and FACS info.
 */
public class DBUserModel {
    /**
     * User data.
     */
    private String username = "";
    private int pin;
    
    public DBUserModel() {
    }
    
    /**
     * Class constructor method
     * @param username
     */
    public DBUserModel(String username) {
        this.username = username;
        
        /* Generate unique long pin number */
        Random randHexaNum = new Random(System.currentTimeMillis());
        this.pin = randHexaNum.nextInt(Integer.MAX_VALUE);
    }

    /**
     * Get username property.
     * @return username (String)
     */
    public String getUsername() {
        return this.username;
    }
    
    /**
     * Get pin property.
     * @return username (int)
     */
    public long getPin() {
        return this.pin;
    }
}