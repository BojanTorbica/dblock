/*
 * Copyright (C) 2015 no-one
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Use and modify this application as you see fit! :)
 */
package model;

import java.io.File;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 */
public class DBUserRegistry {
    private ConcurrentHashMap<Integer, DBUserModel> userRegistry = null;
    private int lastIndex = -1;
    private String currentUsername = "";
    private int size = 0;
    
    public DBUserRegistry() {
        this.userRegistry = new ConcurrentHashMap<>();
        this.lastIndex = -1;
    }
    
    public void setSizeOfROI(int size) {
        this.size = size;
    }
    
    public int getSizeOfROI() {
        return this.size;
    }
    
    public void addUser(DBUserModel newUser, int index) {
        this.userRegistry.put(index, newUser);
        this.lastIndex = index;
        boolean isCreated = new File("src/resources/users/" + newUser.getUsername() + "/templates").mkdirs();

        if (isCreated) {
            System.out.println("Created a user directory.");
        }
    }
    
    public DBUserModel getUser(int index) {
        if (this.userRegistry.containsKey(index)) {
            return this.userRegistry.get(index);
        } else {
            return null;
        }
    }
    
    public int getLastIndex() {
        return this.lastIndex;
    }
    
    public void setCurrentUser(String username) {
        this.currentUsername = username;
    }
    
    public String getCurrentUser() {
        return this.currentUsername;
    }
}