/*
 * Copyright (C) 2015 torbi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package controller;

import application.DBApplicationCore;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * 
 */
public class DBUserMessageController implements Initializable {
    @FXML
    Label message = new Label();
    
    private boolean fromLogin = false;
    
    public void setMessageText(String text) {
        this.message.setText(text);
    }
    
    public String getMessageText() {
        return this.message.getText();
    }
    
    public void setFromLogin(boolean fromLogin) {
        this.fromLogin = fromLogin;
    }
    
    public boolean isFromLogin() {
        return this.fromLogin;
    }
    
    @FXML
    void btnOnContinue(ActionEvent event) {
        DBApplicationCore.userNewVerification.set(false);
        DBApplicationCore.loginVerification.set(false);
        DBApplicationCore.message.set(false);
        DBApplicationCore.successLogin.set(true);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
}