/*
 * Copyright (C) 2015 no-one
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Use and modify this application as you see fit! :)
 */
package controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import application.DBApplicationCore;

/**
 * Controller class for user data model.
 */
public class DBEntryController implements Initializable {
    @FXML
    Button btnLogIn;
    @FXML
    Button btnNewAccount;
   
    @FXML
    void onBtnLogInClick(ActionEvent event) {
        System.out.println("Log in button clicked");
        DBApplicationCore.logIn.set(true);
    }
    
    @FXML
    void onBtnNewAccountClick(ActionEvent event) {
        System.out.println("New account button clicked");
        DBApplicationCore.userNew.set(true);
    }
    
    @FXML
    void onBtnExitClick(ActionEvent event) {
        System.out.println("Exit button clicked");
        DBApplicationCore.exitApp.set(true);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // TO DO
    }
}