/*
 * Copyright (C) 2015 no-one
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Use and modify this application as you see fit! :)
 */
package controller;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import java.sql.SQLException;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;

import static org.bytedeco.javacpp.opencv_imgcodecs.*;
import org.bytedeco.javacpp.opencv_core.Rect;

import application.DBApplicationCore;
import application.database.DBConnection;
import java.util.Arrays;
import model.DBUserModel;
import org.bytedeco.javacpp.opencv_core.Mat;
/**
 *
 */
public class DBUserNewVerificationController implements Initializable {
    @FXML
    Button btnExpressionCapture;

    @FXML
    Button btnCancel;

    @FXML
    Button btnOpenStream;

    @FXML
    ImageView imageView;
    
    @FXML
    void onBtnOpenStream(ActionEvent event) {
        System.out.println("Open stream button clicked");
        /*
        * Open video input stream (web cam resource)
        */
        DBApplicationCore.thread.setImageView(imageView);
        DBApplicationCore.t = new Thread(DBApplicationCore.thread);
        
        DBApplicationCore.t.setDaemon(true);
        DBApplicationCore.thread.setRunnable(true);
        DBApplicationCore.t.start();
    }
    
    @FXML
    void onBtnExpressionCapture(ActionEvent event) {
        System.out.println("Expression capture button clicked");
        /*
        * Check if video stream is oppened, if yes capture and save current frame
        * as a facial expression passcode, if not offer the user dialog to open
        * video stream from a resource (web cam).
        */
        if (DBApplicationCore.thread.isThreadRunning()) {
            int index = DBApplicationCore.registry.getLastIndex();
            
            DBUserModel user = DBApplicationCore.registry.getUser(index);
            String username = user.getUsername();
            long pin = user.getPin();

            String userExpression = "0" + index + "_" + username + "_expression_" + pin;
            String passcodePath = "src/resources/users/" + username + "/" + userExpression + ".bmp";

            boolean foundFace = true; // For now...
            boolean didWrite = false;
            if (foundFace) {
                Mat face = new Mat();
                int size = 0;
                do {
                    face = DBApplicationCore.thread.getDetectedFace(); 
                    size = face.cols();
                } while (face.cols() == 0);
                
                didWrite = imwrite(passcodePath, face);
                if (didWrite) {
                    DBApplicationCore.thread.segmentTemplate(face, 4, username);
                    this.writeOutBoundingBoxAndSize(DBApplicationCore.thread.getBoundingBox(), size, username);

                    try {
                        String insertQuerry = "update users set passcode='" + passcodePath + "'" + " where id=" + index;
                        DBConnection.dbQuery(insertQuerry, DBApplicationCore.connection, false);
                        System.out.println("Path saved to database");
                    } catch (SQLException ex) {
                        Logger.getLogger(DBUserNewVerificationController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    DBUserMessageController verified = new DBUserMessageController();
                    verified.setMessageText("New user registered succesfully!\n Welcome!");
                    verified.setFromLogin(false);
                }
            }
        }
    }
    
    @FXML
    void onBtnCancel(ActionEvent event) {
        System.out.println("Cancel button clicked");
        
        if (DBApplicationCore.thread.isThreadRunning()) {
            DBApplicationCore.thread.setRunnable(false);
            DBApplicationCore.thread.closeStream();
        }
        
        DBApplicationCore.userNewVerification.set(false);
    }
    
    private void writeOutBoundingBoxAndSize(Rect boundingBox, int size, String username) {
        try {
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("src/resources/users/" + username + "/" + username + "_face_bounding_box.txt"), "utf-8"))) {
                int[] bbox = new int[8];
                
                bbox[0] = boundingBox.x();
                bbox[1] = boundingBox.y();
                bbox[2] = boundingBox.x()+boundingBox.width();
                bbox[3] = boundingBox.y();
                bbox[4] = boundingBox.x()+boundingBox.width();
                bbox[5] = boundingBox.y()+boundingBox.height();
                bbox[6] = boundingBox.x();
                bbox[7] = boundingBox.y()+boundingBox.height();
                
                System.out.println("BoundingBox: " + Arrays.toString(bbox));
                writer.write("Size: " + size);
                writer.write(System.lineSeparator());
                writer.write(Arrays.toString(bbox));
            }
        } catch (IOException ex) {
            Logger.getLogger(DBUserNewVerificationController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // TO DO
    }
}