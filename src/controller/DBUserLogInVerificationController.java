/*
 * Copyright (C) 2015 no-one
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Use and modify this application as you see fit! :)
 */
package controller;


import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.fxml.Initializable;

import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;
import org.bytedeco.javacpp.opencv_core.Mat;

import application.DBApplicationCore;
import application.database.DBConnection;
/**
 *
 */
public class DBUserLogInVerificationController implements Initializable {
    @FXML
    Button btnCancel;
    @FXML
    Button btnStart;
    @FXML
    ImageView imageView;
    
    @FXML
    void onBtnCancel(ActionEvent event) {
        System.out.println("Cancel button clicked");
        
        if (DBApplicationCore.thread.isThreadRunning()) {
            DBApplicationCore.thread.setRunnable(false);
            DBApplicationCore.thread.closeStream();
        }
        
        DBApplicationCore.loginVerification.set(false);
    }
    
    @FXML
    void onBtnCaptureExpression(ActionEvent event) {
        System.out.println("Capture expression for verification button clicked");
        /* Retrieve id and pin of user from the database */
        String sqlQuery = "select * from users";
        String expressionToVerify = "";
        String expressionForVerification = "";
        String user = DBApplicationCore.registry.getCurrentUser();

        if (DBApplicationCore.thread.isThreadRunning()) {
            try {
                ResultSet results = DBConnection.dbQuery(sqlQuery, DBApplicationCore.connection, true);
                while (results.next()) {
                    String name = results.getString("username");
                    if (name.equals(user)) {
                        System.out.println("User found retrieving passcode info.");
                        expressionForVerification = results.getString("passcode");
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(DBUserLogInVerificationController.class.getName()).log(Level.SEVERE, null, ex);
            }
            expressionToVerify = expressionForVerification.replace(".bmp", "_toverify_.bmp");

            boolean foundFace = true; // For now...
            boolean didWrite = false;
            if (foundFace) {
                Mat face = new Mat();DBApplicationCore.thread.getCurrentFrame();
                Mat faceROI = new Mat();DBApplicationCore.thread.getDetectedFace();
                do {
                    face = DBApplicationCore.thread.getCurrentFrame();
                    faceROI = DBApplicationCore.thread.getDetectedFace();
                } while (!(faceROI.cols() == faceROI.rows()));
                
                didWrite = imwrite(expressionToVerify, faceROI);

                System.out.println("Expression for verification saved, running application to verify passcode.");
                Mat[] templates = DBApplicationCore.thread.getTemplates(user, 4);
                boolean isMatched = DBApplicationCore.thread.match(faceROI, templates, -1);
                
                DBUserMessageController verified = new DBUserMessageController();
                if (isMatched) {
                    verified.setMessageText("Login successfull!\n Welcome!");
                    verified.setFromLogin(true);
                    DBApplicationCore.message.set(true);
                } else {
                    verified.setMessageText("Login failed, please try again!\n Welcome!");
                    verified.setFromLogin(true);
                    DBApplicationCore.message.set(true);
                }
            }
        }
    }
    
    @FXML
    void onBtnStart(ActionEvent event) {
        System.out.println("Start button clicked");
        /*
        * Open video input stream (web cam resource), and continuously detect
        * whether or not the user is activating the appropriate expression passcode.
        */
        DBApplicationCore.thread.setImageView(imageView);
        DBApplicationCore.t = new Thread(DBApplicationCore.thread);
        
        DBApplicationCore.t.setDaemon(true);
        DBApplicationCore.thread.setRunnable(true);
        DBApplicationCore.t.start();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //TO DO
    }
}
