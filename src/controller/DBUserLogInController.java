/*
 * Copyright (C) 2015 no-one
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Use and modify this application as you see fit! :)
 */
package controller;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.fxml.Initializable;

import application.DBApplicationCore;

/**
 *
 */
public class DBUserLogInController implements Initializable {
    @FXML
    TextField usernameField;
    
    @FXML
    void onBtnLoginWindowCancelClick(ActionEvent event) {
        System.out.println("Cancel button clicked");
        DBApplicationCore.logIn.set(false);
    }
    
    @FXML
    void fieldFocusGained() {
        this.usernameField.setText("");
        this.usernameField.setStyle("-fx-text-fill: black;");
    }
    
    @FXML
    void onBtnLoginWindowVerificationClick(ActionEvent event) {
        System.out.println("Proceed button clicked");
        
        /* Open input video stream */
        DBApplicationCore.thread.openStream();
        
        String username = this.usernameField.getText();
        String sql = "select * from users where username=?";
        
        try {
            if (!"".equals(username)) {
                System.out.println("Username: " + username);

                // Verify that the user exists
                PreparedStatement statement = DBApplicationCore.connection.prepareStatement(sql);
                statement.setString(1, username);
                
                ResultSet result = statement.executeQuery();
                
                if (result.next()) {
                    DBApplicationCore.loginVerification.set(true);
                    DBApplicationCore.logIn.set(false);
                    DBApplicationCore.registry.setCurrentUser(username);
                } else {
                    System.out.println("Unable to find username, please try again.");
                    this.usernameField.setText("Unable to find username, please try again.");
                    this.usernameField.setStyle("-fx-text-fill: red;"
                                                + "\n -fx-border-color: red;"
                                                + "-fx-focus-color: red;");
                }
            } else {
                System.out.println("Username field cannot be empty.");
                this.usernameField.setPromptText("Username field cannot be empty.");
                this.usernameField.setStyle("-fx-text-fill: red;"
                                            + "\n -fx-border-color: red;"
                                            + "-fx-focus-color: red;");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBUserLogInController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //TO DO
    }
}
