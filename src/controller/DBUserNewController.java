/*
 * Copyright (C) 2015 no-one
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Use and modify this application as you see fit! :)
 */
package controller;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.sql.SQLException;
import java.sql.ResultSet;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.fxml.Initializable;

import model.DBUserModel;
import application.DBApplicationCore;
import application.database.DBConnection;

/**
 *
 */
public class DBUserNewController implements Initializable {
    @FXML
    TextField userNewUsername; 
    
    @FXML
    void fieldFocusGained() {
        this.userNewUsername.setText("");
        this.userNewUsername.setStyle("-fx-text-fill: black;");
    }
    
    @FXML
    void onBtnUserNewWindowCancelClick(ActionEvent event) {
        System.out.println("Cancel newUser button clicked");
        DBApplicationCore.userNew.set(false);
    }
    
    private String getUserNewName() {
        return this.userNewUsername.getText();
    }
    
    @FXML
    void onBtnUserNewWindowVerificationClick(ActionEvent event) {
        System.out.println("Proceed button clicked");
        String username = this.getUserNewName();
        
        /* Open input video stream */
        DBApplicationCore.thread.openStream();
        
        /* Create a new user */
        DBUserModel newUser = new DBUserModel(username);
        
        /* Prepare an sql querry */
        String insertQuerry = "insert into users (username,pin) values('"
                              + newUser.getUsername() + "'," + newUser.getPin()+")";

        try {
            if (!"".equals(username)) {
                System.out.println("Username: " + username);
                
                /* Get all the id's in the database */
                ResultSet keys = DBConnection.dbQuery(insertQuerry, DBApplicationCore.connection, false);
                
                /* Get the last id */
                int increment = -1;
                if (keys.next()) {             
                    increment = keys.getInt(1);
                }
                
                /* Register the user in the registry with the appropriate id */
                DBApplicationCore.registry.addUser(newUser, increment);
                DBApplicationCore.userNewVerification.set(true);
                DBApplicationCore.userNew.set(false);
            } else {
                System.out.println("Username field cannot be empty.");
                this.userNewUsername.setPromptText("Username field cannot be empty.");
                this.userNewUsername.setStyle("-fx-text-fill: red;"
                                            + "\n -fx-border-color: red;"
                                            + "-fx-focus-color: red;");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBUserNewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // TO DO
    }
}
